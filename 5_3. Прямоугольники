//Дана последовательность N прямоугольников различной ширины и высоты (wi, hi). 
//Прямоугольники расположены, начиная с точки (0, 0), на оси ОХ вплотную друг за другом (вправо).
//Требуется найти M - площадь максимального прямоугольника (параллельного осям координат), 
//который можно вырезать из этой фигуры.
//Время работы - O(n).

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <memory.h>

typedef struct BlockData
{
	int height;
	int boundary;
} BlockData;

typedef struct Stack
{
	int size;
	int capacity;
	BlockData* data;
} Stack;

typedef struct Position
{
	int left;
	int right;
} Position;

typedef struct AreaData
{
	int left;
	int right;
	int height;
} AreaData;

// Stack Functions
void       Init(Stack* stack);
void       Destroy(Stack* stack);
void       Push(Stack* stack, BlockData elem);
BlockData  Pop(Stack* stack);
BlockData  Top(Stack* stack);

// Data Processing Functions
void FromLeftToRight(AreaData* areaData, Position* blocks, Stack* handyStack, int n);
void FromRightToLeft(AreaData* areaData, Position* blocks, Stack* handyStack, int n);
int  CalculateMaxArea(AreaData* areaData, int n);

int main()
{
	Stack handyStack;
	Init(&handyStack);

	int n = 0;
	scanf("%d", &n);
	Position* blocks = (Position*)calloc(n, sizeof(Position));
	AreaData* areaData = (AreaData*)calloc(n, sizeof(AreaData));
	assert(n>0 && blocks != NULL && areaData != NULL);

	// blocks[0].left = 0;
	for (int i = 0; i < n; i++)
	{
		int width = 0, height = 0;
		scanf("%d %d", &width, &height);
    	assert(width >= 0 && height >= 0);
		if (i > 0)
			blocks[i].left = blocks[i - 1].right;
		blocks[i].right = blocks[i].left + width;
		areaData[i].height = height;
	}

	FromLeftToRight(areaData, blocks, &handyStack, n);
	handyStack.size = 0;
	FromRightToLeft(areaData, blocks, &handyStack, n);

	printf("%d", CalculateMaxArea(areaData, n));

	free(blocks);
	free(areaData);
	Destroy(&handyStack);

	return 0;
}

//------------------------------------------------------
//-----------------  DEFINITONS !!!!!!!! ---------------
//------------------------------------------------------

void Init(Stack* stack)
{
	stack->size = 0;
	stack->capacity = 0;
}

void Destroy(Stack* stack)
{
	free(stack->data);
	stack->size = 0;
	stack->capacity = 0;
}

BlockData Top(Stack* stack)
{
	return stack->data[stack->size - 1];
}

void Push(Stack* stack, BlockData elem)
{
	if (stack->capacity - stack->size < 1)
	{
		BlockData* data = (BlockData*)calloc((stack->size + 1) * 2, sizeof(BlockData));
		assert(data != NULL);
		memcpy(data, stack->data, stack->size * sizeof(BlockData));

		if (stack->capacity != 0)
			free(stack->data);
		stack->data = data;
		stack->capacity = (stack->size + 1) * 2;
	}
	stack->data[stack->size] = elem;
	stack->size++;
}

BlockData Pop(Stack* stack)
{
	stack->size--;
	return stack->data[stack->size];
}

//-------------------------------------------------------------
//-------------------------------------------------------------

void FromLeftToRight(AreaData* areaData, Position* blocks, Stack* handyStack, int n)
{
	for (int i = 0; i < n; i++)
	{
		BlockData newBlock;
		newBlock.height = areaData[i].height;
		newBlock.boundary = blocks[i].right;

		while (handyStack->size > 0 && Top(handyStack).height >= areaData[i].height)
			Pop(handyStack);

		int boundary = 0;
		if (handyStack->size != 0)
			boundary = Top(handyStack).boundary;

		Push(handyStack, newBlock);
		areaData[i].left = boundary;
	}
	return;
}

void FromRightToLeft(AreaData* areaData, Position* blocks, Stack* handyStack, int n)
{
	for (int i = n - 1; i >= 0; i--)
	{
		BlockData newBlock;
		newBlock.height = areaData[i].height;
		newBlock.boundary = blocks[i].left;

		while (handyStack->size > 0 && Top(handyStack).height >= areaData[i].height)
			Pop(handyStack);

		int boundary = blocks[n - 1].right;
		if (handyStack->size != 0)
			boundary = Top(handyStack).boundary;

		Push(handyStack, newBlock);
		areaData[i].right = boundary;
	}
	return;
}

int CalculateMaxArea(AreaData* areaData, int n)
{
	int maxArea = 0;
	for (int i = 0; i < n; i++)
	{
		int blockArea = areaData[i].height * (areaData[i].right - areaData[i].left);
		maxArea = (blockArea > maxArea) ? blockArea : maxArea;
	}
	return maxArea;
}